**CS: GO: M4A4 Howl продан коллекционеру за 655 тысяч реалов**

Датчанин Luksusbums приобретает еще один предмет для коллекции миллионеров в рамках FPS от Valve 

Один из величайших коллекционеров Counter-Strike: Global Offensive (CS: GO) в мире, датский Luksusbums купил на этой неделе скин M4A4 Howl за 122 тысячи долларов. Пистолет украшен четырьмя голографическими наклейками от iBUYPOWER, одной из самых редких и дорогих в мире и частой целью коллекционеров косметики Valve для шутеров от первого лица. 

<img class="featurable" style="max-height:300px;max-width:400px;" itemprop="image" src="https://img.republicworld.com/republic-prod/stories/promolarge/xxhdpi/7frttr1ttyopr5ft_1586336733.jpeg?tr=w-758,h-433"/><br><br>
Какой город вдохновил каждую карту CS: GO? 

У Неймара есть инвентарь скинов на сумму 210 тысяч реалов. 

Согласно профилю ohnePixel, который специализируется на маркетинге редких скинов Counter-Strike, M4A4, приобретенный датчанином, представляет собой самую дорогую продажу в истории, совершенную через торговую платформу. Это связано с тем, что, как правило, великие коллекционеры косметики из игры обычно используют китайскую платформу BUFF для продаж и покупки интересных предметов, которые реже встречаются в сообществе. 

На уровне сохранности M4A4 Howl, приобретенный датчанином, имеет один из самых низких показателей с расходами 0,004. Кроме того, еще больше увеличивает ценность косметики, это StatTrak. То есть каждый убой, проведенный с его помощью, засчитывается маркером. На некоторых сайтах, специализирующихся на продаже скинов Counter-Strike, каждая наклейка может стоить более 1000 реалов, а самый доступный скин M4 продается по цене более 24 000 реалов. 

Это лишь одна из многих коллекционных сделок [статистика кс го](https://esportsresults.net/cs-go/tournaments). Среди приобретенных им предметов - закаленный чехол для АК-47 с четырьмя голографическими наклейками от Titan. По данным ohnePixel, вооружение было куплено за 150 тысяч долларов США, что эквивалентно более чем 800 тысячам реалов, и сделка была совершена на вышеупомянутой платформе для продажи и покупки более редких скинов. 

**CS: GO: M4A4 Howl sold to collector for 655 thousand reais**

Dane Luksusbums Acquires Another Item for Valve's FPS Millionaire Collection 

One of the greatest collectors of Counter-Strike: Global Offensive (CS: GO) in the world, Danish Luksusbums bought the M4A4 Howl skin for $ 122,000 this week. The pistol is adorned with four holographic stickers from iBUYPOWER, one of the rarest and most expensive in the world and a frequent target of collectors of Valve first-person shooter cosmetics. 

Which city inspired each CS: GO map? 

Neymar has an inventory of R $ 210,000 worth of skins. 

According to the profile of ohnePixel, which specializes in marketing rare Counter-Strike skins, the M4A4 acquired by the Dane represents the most expensive sale in history made through a trading platform. This is because, as a rule, great collectors of cosmetics from the game usually use the Chinese BUFF platform to sell and buy interesting items that are less common in the community. 

At the state of preservation, the M4A4 Howl, acquired by the Dane, has one of the lowest rates with costs of 0.004. It also further increases the value of cosmetics, this is StatTrak [https://esportsresults.net/cs-go/tournaments](https://esportsresults.net/cs-go/tournaments). That is, every slaughter carried out with its help is counted with a marker. On some sites specializing in Counter-Strike skins, each sticker can cost over R $ 1,000, and the most affordable M4 skin sells for over R $ 24,000. 

<img class="featurable" style="max-height:300px;max-width:400px;" itemprop="image" src="https://www.hiddenpath.com/wp-content/uploads/2017/05/HP_Blog_CSGO-1_550x300_acf_cropped.jpg"/><br><br>

This is just one of many collectible deals. Among the items he purchased is a tempered AK-47 case with four Titan holographic decals. According to ohnePixel, the weapon was purchased for $ 150,000, the equivalent of over 800,000 reais, and the deal was made on the aforementioned platform to sell and buy rarer skins. 
